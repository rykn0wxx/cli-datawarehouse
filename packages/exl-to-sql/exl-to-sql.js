const XLSX = require('xlsx')
const _ = require('lodash')

class ExlToSql {
  constructor (fileObj, config) {
    this.fileType = fileObj.fileType
    this.fileLoc = fileObj.fileLoc
    this.config = config
  }

  async init () {
    this.wb = await this._readFile(this.fileLoc, this.config)
    this.csv = await this._toCsv(this.wb)
    return await this
  }

  async _readFile (xlFile, xlConfig) {
    return await XLSX.readFile(`${xlFile}`, xlConfig)
  }

  async _toCsv (wb) {
    const result = []
    result.push(XLSX.utils.sheet_to_csv(wb.Sheets[wb.SheetNames[0]]))
    return _.join(result, '\n')
  }
}

module.exports = ExlToSql
