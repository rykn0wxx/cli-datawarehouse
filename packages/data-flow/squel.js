module.exports = (sequelize) => {
  return {

    query (qrySql, opts) {
      const qryOpts = {
        ...opts,
        logging: false,
        raw: true
      }
      return sequelize.query(qrySql, qryOpts)
    },

    storedProc (procName) {
      return sequelize.query(`EXEC ${procName}`, {
        logging: false
      })
    },

    insertQuery (qry) {
      return sequelize.query(qry, {
        raw: true,
        logging: false,
        type: sequelize.QueryTypes.INSERT
      })
    }
  }

}
