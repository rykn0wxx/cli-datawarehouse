const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')

const config = require('../../../lib/config')

const sequelize = new Sequelize(config.mssql)
const db = {}

fs.readdirSync(__dirname).filter((file) => file !== 'index.js').forEach((file) => {
  const model = sequelize.import(path.join(__dirname, file))
  console.log(model)
  db[model.name] = model
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
