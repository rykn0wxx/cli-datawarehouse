const Sequelize = require('sequelize')
const _ = require('lodash')

const exlToSql = require('../exl-to-sql/exl-to-sql')
const { files, colors } = require('../shared-utils')
const storedProcs = require('./stored-procs')

const cl = console.log

const sqlSync = async (sequelize) => {
  const result = await sequelize.sync({
    logging: true,
    force: false
  })
  return result
}

class DataFlow {
  constructor (config) {
    this.config = config
    const { sequelize } = require('./models')
    this.sequelize = sequelize
  }

  async init (fileObj) {
    if (files.fileExist(fileObj.fileLoc)) {
      this.fileObj = fileObj

      this.ExlToSql = new exlToSql(fileObj, this.config.xlsx)
      await this.ExlToSql.init()

      this.squel = require('./squel')(this.sequelize)
      await sqlSync(this.sequelize)
      return await this
    } else {
      throw new Error(`Process exited: file provided not found`)
    }
  }

  async preImport () {
    let success = 0
    cl(colors.info(`Started running pre import processes`), _.now())
    await Sequelize.Promise.reduce(_.get(storedProcs, `uspArr.${this.fileObj.fileType}`), (ind, usp) => {
      return this.squel.storedProc(usp).then(() => {
        return ind + 1
      })
    }, 0).then((ind) => {
      success = ind
    }).finally(() => {
      cl(colors.info(`Ended ran ${success} pre import processes`))
    })
  }

  getImpTktTbl () {
    const sequelize = this.sequelize
    class ImportTicketTbl extends Sequelize.Model { }
    ImportTicketTbl.init({
      tmp_parent_project: Sequelize.STRING,
      tmp_project_name: Sequelize.STRING,
      tmp_timezone: Sequelize.STRING,
      tmp_call_category: Sequelize.STRING,
      tmp_call_type: Sequelize.STRING,
      tmp_call_action: Sequelize.STRING,
      tmp_call_action_reason: Sequelize.STRING,
      tmp_disp: Sequelize.STRING,
      tmp_service_id: Sequelize.INTEGER,
      tmp_service_name: Sequelize.STRING,
      tmp_orig_srv: Sequelize.INTEGER,
      tmp_user_id: Sequelize.STRING,
      tmp_ivr_param_1: Sequelize.STRING,
      tmp_ivr_param_2: Sequelize.STRING,
      tmp_ivr_param_3: Sequelize.STRING,
      tmp_ivr_param_4: Sequelize.STRING,
      tmp_ivr_param_9: Sequelize.STRING,
      tmp_ivr_param_10: Sequelize.STRING,
      tmp_ivr_param_16: Sequelize.STRING,
      tmp_ivr_param_18: Sequelize.STRING,
      tmp_ivr_param_19: Sequelize.STRING,
      tmp_call: Sequelize.INTEGER,
      tmp_seq: Sequelize.INTEGER,
      tmp_dialed_num: Sequelize.STRING,
      tmp_answer_date: Sequelize.DATE,
      tmp_end_date: Sequelize.DATE,
      tmp_end_date_tz: Sequelize.DATE,
      tmp_start_date: Sequelize.DATE,
      tmp_start_date_tz: Sequelize.DATE,
      tmp_wrap_end_date: Sequelize.DATE,
      tmp_hold_number: Sequelize.INTEGER,
      tmp_queue_time: Sequelize.INTEGER,
      tmp_talk_time: Sequelize.INTEGER,
      tmp_hold_time: Sequelize.INTEGER,
      tmp_wrap_time: Sequelize.INTEGER
    }, { sequelize, modelName: 'import_ticket_tbl', timestamps: false })
    return ImportTicketTbl
  }
}

module.exports = DataFlow
