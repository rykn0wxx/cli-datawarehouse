module.exports = {
  uspArr: {
    phone: ['uspDeleteTmpFactCall', 'uspResetTmpFactIdentity', 'uspTruncateImportPhoneTbl'],
    ticket: ['uspDeleteTmpFactTicket', 'uspResetTmpFactTcktIdentity', 'uspTruncateImportTicketTbl']
  },
  uspImportToTmp: {
    phone: 'uspInsertImportToTmp',
    ticket: 'uspInsertImportToTmpTicket'
  },
  uspTmpToFact: {
    phone: '',
    ticket: ''
  }
}
