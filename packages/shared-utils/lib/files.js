const fs = require('fs')
const path = require('path')

exports.files = {
  getCurDirBase () {
    return path.basename(process.cwd())
  },
  dirExist (filePath) {
    try {
      return fs.statSync(filePath).isDirectory()
    } catch (e) {
      return false
    }
  },
  fileExist (filePath) {
    try {
      return fs.statSync(filePath).isFile()
    } catch (e) {
      return false
    }
  }
}
