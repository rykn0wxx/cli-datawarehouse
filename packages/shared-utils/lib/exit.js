exports.exit = ({ number, message}) => {
  if (number > 0) {
    console.error(`Process exited with error: ${message}`)
    process.exit(number)
  } else {
    console.log(`Done!`)
    process.exit()
  }
}
