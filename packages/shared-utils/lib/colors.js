const colors = require('colors/safe')

colors.setTheme({
  silly: 'rainbow',
  verbose: 'grey',
  error: ['bold', 'red'],
  info: ['bold', 'blue']
})

// exports.colors = (type, msg) => {
//   return colors[type](msg)
// }
exports.colors = colors
