const { exit } = require('./exit')
const { colors } = require('./colors')
const cl = console.log

exports.testConnect = async (config) => {
  const Sequelize = require('sequelize')
  const sequelize = new Sequelize(config)
  await sequelize.authenticate().then(() => {
    cl(colors.info(`Connected successfully`))
    exit(0)
  }).catch(err => {
    cl(colors.error(`Error testing connection`), err)
  }).finally(() => {
    sequelize.close()
  })
}
