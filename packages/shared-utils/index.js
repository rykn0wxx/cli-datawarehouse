[
  'exit',
  'testConnect',
  'colors',
  'files'
].forEach(m => {
  Object.assign(exports, require(`./lib/${m}`))
})
