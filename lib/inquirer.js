const inquirer = require( 'inquirer' )

function valResp ( val ) {
  if (val.length) {
    return true
  } else {
    return 'Did you enter anything...'
  }
}

module.exports = {
  async askWhatToDo () {
    return await inquirer.prompt([{
      name: 'todo',
      type: 'list',
      message: 'What would you like to do? ',
      choices: [ 'Test Connection', 'Import Data' ],
      validate: valResp
    }])
  },

  async askFileDetails () {
    return await inquirer.prompt([
      {
        name: 'fileType',
        type: 'list',
        message: 'What type of data will you upload? ',
        choices: ['phone', 'ticket'],
        validate: valResp
      },
      {
        name: 'fileLoc',
        type: 'input',
        message: 'Enter the full path where the file is located: ',
        validate: valResp
      }
    ])
  }
}
