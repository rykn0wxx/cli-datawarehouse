module.exports = {
  mssql: {
    host: 'localhost',
    database: 'SandBoxDB',
    username: 'sa',
    password: '666123',
    port: 62412,
    dialect: 'mssql',
    dialectOptions: {
      requestTimeout: 90000,
      timeout: 90000
    },
    logging: false
  },
  xlsx: {
    type: 'binary',
    cellNF: true,
    cellDates: true
  },
  chunkSize: 2
}

