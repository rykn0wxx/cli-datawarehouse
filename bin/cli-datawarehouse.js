#!/usr/bin/env node

const clear = require('clear')
const figlet = require('figlet')

const cl = console.log

const inquirer = require('../lib/inquirer')
const config = require('../lib/config')

const { colors, testConnect, exit } = require('../packages/shared-utils')
const dataFlow = require('../packages/data-flow')

clear()
cl(
  colors.silly(figlet.textSync('Datawarehouse CLI', { horizontalLayout: 'full' }))
)

const runTestConnection = async () => {
  await testConnect(config.mssql)
}

const runNormal = async () => {
  try {
    const fileDetails = await inquirer.askFileDetails()
    const DataFlow = new dataFlow(config)
    await DataFlow.init(fileDetails)
    // await DataFlow.preImport()
    exit(0)
  } catch (err) {
    cl(colors.error(`Something went wrong`), err)
  }
}

const runApp = async () => {
  try {
    const taskTodo = await inquirer.askWhatToDo()
    if ( taskTodo.todo === 'Test Connection') {
      runTestConnection()
    } else {
      runNormal()
    }
  } catch (error) {
    cl(`Unexpected error occured`, error)
  }
}

runApp()
